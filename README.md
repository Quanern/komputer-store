# Komputer Store
This project is a webpage called Komputer Store. In this store you are able to earn money by working, sending your
hard-earned money to the bank, apply for a loan, fall in debt, browse the store for top-end laptops and buy said laptop.

The application is deployed on heroku: http://quans-komputer-store.herokuapp.com/

The project is separated into 3 modules:
1. Bank
2. Work
3. Laptop (The store)

## Bank
This module is your bank account. It will show your balance, your debt if you have any, and a button to apply for a 
loan. 

The requirements to get a loan are not too strict, but you can not loan more than twice the amount you have in 
your balance, and no more than one loan at any given time. The button will be hidden by default whenever you have
a loan, but if you by any means gets the button to show up again, the loan won't go through because you already
have a loan you haven't repaid.

## Work
This module is your home office. Here you can work to earn money for your next paycheck, send the paycheck to the bank
or use it all to pay off your loan. The paycheck is not a part of your balance until you send it to the bank.

If you have debt, 10% will be deducted from your paycheck to pay it off, and the remaining 90% will go to your balance.
If 10% of your paycheck is more than the remainder of the loan, the difference will be added to your balance.

If you use all of your paycheck to pay off the debt, none of the paycheck will go to your balance. If your paycheck is
more than the remainder of the loan, the difference will be added to your balance.

## Laptop
This module is the store itself, containing a select box with the most amazing laptops you can find. By selecting a
laptop from the select box, a new and bigger box will show up beneath the three modules and will give you more 
information about your laptop, an image of it, it's price, and a button to buy it. If you do not have enough money to
buy the laptop, either work more or apply for a loan.