const elBalance = document.getElementById('bank-balance-amount')
const elLoanAmount = document.getElementById('loan-amount')
const elLoanText = document.getElementById('loan-text')

const elLoanMoneyButton = document.getElementById('get-loan-button')

const elPayBackLoanButton = document.getElementById('pay-back-loan-button')

/**
 * Class with bank actions
 */
class Bank {
    balance = 0
    loan = 0

    /**
     * Adds money to the user's account
     * @param amount is the amount of money that is to be added to the account
     */
    addBalance(amount) {
        this.balance += amount
        elBalance.innerText = this.balance + ' Kr'
    }

    /**
     * Checks if the amount is a valid amount to get a loan or not.
     * @param amount is the amount of money the user wants to loan.
     * @returns {boolean} whether or not the amount is an appropriate amount for the user to loan.
     */
    checkIfGoodForLoan(amount) {
        return (amount <= this.balance * 2 && amount > 0)
    }

    /**
     * The user applies for a loan
     * @param amount is how much money the user is loaning
     */
    applyForLoan(amount) {
        this.loan = amount
        this.addBalance(amount)
        elLoanAmount.innerText = this.loan + ' Kr'
    }

    /**
     * Hides the loan button, shows the pay back loan button, the debt and a text saying loan.
     */
    hideLoanButtonAndShowLoan() {
        elLoanText.style.display = ''
        elLoanAmount.style.display = ''
        elPayBackLoanButton.style.display = ''
        elLoanMoneyButton.style.display = 'none'
    }

    /**
     * Pays the loan
     * @param amount of money being deducted from the debt
     */
    payLoan(amount) {
        this.loan -= amount
        elLoanAmount.innerText = this.loan + ' Kr'
    }

    /**
     * Gets the loan
     * @returns {number} the loan
     */
    getLoan() {
        return this.loan
    }

    /**
     * Checks if loan exists or not
     * @returns {boolean} the loan
     */
    loanExists() {
        return this.loan > 0
    }

    /**
     * Gets the balance
     * @returns {number} the balance
     */
    getBalance() {
        return this.balance
    }

}

export default Bank