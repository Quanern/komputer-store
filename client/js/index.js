import Bank from "./bank.js";
import Work from "./work.js"
import Laptop from "./laptop.js";

const elLoanMoneyButton = document.getElementById('get-loan-button')
const elEarnMoneyButton = document.getElementById('earn-money-button')
const elMoveMoneyToBankButton = document.getElementById('move-to-bank-button')
const elPayBackLoanButton = document.getElementById('pay-back-loan-button')
const elBuyLaptopButton = document.getElementById('buy-laptop-button')

const elSelectLaptop = document.getElementById('laptop-selector')

const bank = new Bank()
const work = new Work()
const laptop = new Laptop()
/**
 * EventListener for Loan Money button. When clicked it will come a prompt asking for how much you want to loan.
 * Checks if the amount is 0 (or null) and returns nothing since cancel returns null.
 * Then it checks if the amount is a valid number between 0 and up to double the user's balance.
 * If the amount is OK, the loan will go through, the apply for a loan button will be hidden, and the debt will show up
 */
elLoanMoneyButton.addEventListener('click', function () {
    const amount = prompt('How big a loan are you applying for?', '1000')
    if(Number(amount) === 0){
        return
    }
    // If the user is a hackerman and decides to change the display of the button from hidden to being shown,
    // they still won't be to get a new loan since they have one already, then the button will be hidden again
    if(bank.loanExists()){
        alert('You must pay back your current loan before you can apply for another one')
        bank.hideLoanButtonAndShowLoan()
        return
    }
    if (!bank.checkIfGoodForLoan(Number(amount))) {
        alert('You can only apply for a loan equal to double your balance or less.')
        return
    }
    bank.applyForLoan(Number(amount))
    bank.hideLoanButtonAndShowLoan()
})
/**
 * EventListener for the Work button. Will add 100 Kr to your paycheck every time it is clicked.
 */
elEarnMoneyButton.addEventListener('click', function () {
    work.work()
})
/**
 * EventListener for the move money to bank button. 10% of the pay will be deducted to pay off the loan.
 */
elMoveMoneyToBankButton.addEventListener('click', function () {
    work.moveMoneyToBank(bank)
})
/**
 * EventListener for pay all to loan button. 100% of the pay will be used to pay off the loan
 */
elPayBackLoanButton.addEventListener('click', function () {
    work.payLoan(bank)
})
/**
 * EventListener for the select box with laptops. Returns information about the laptop chosen.
 */
elSelectLaptop.addEventListener('change', function () {
    const value = Number(this.value)
    laptop.showLaptop(value)
})
/**
 * EventListener for the buy laptop button. Attempts to buy the laptop.
 */
elBuyLaptopButton.addEventListener('click', function () {
    laptop.buyLaptop(bank)
})