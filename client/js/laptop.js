const elLaptopFeaturesText = document.getElementById('laptop-features-text')
const elLaptopInformationText = document.getElementById('laptop-information-text')
const elLaptopName = document.getElementById('laptop-name')
const elLaptopPrice = document.getElementById('laptop-price')
const elFeatureTitle = document.getElementById('laptop-features-title')

const elSelectLaptop = document.getElementById('laptop-selector')
const elLaptopInformationBox = document.getElementById('laptop-information-box')
const elLaptopImage = document.getElementById('laptop-image')

/**
 * Class for Laptop actions
 */
class Laptop {
    price = 0
    name = ''

    /**
     * Constructor for a laptop object
     * @param id of the computer in the system
     * @param name of the computer
     * @param features that the computer has worth mentioning, eg monitor, keyboard, water resistant
     * @param price of the computer
     * @param picture of the computer
     * @param information about the computer
     */
    constructor(id, name, features, price, picture, information) {
        this.id = id
        this.name = name
        this.features = features
        this.price = price
        this.picture = picture
        this.information = information
    }

    /**
     * Displays information about the selected laptop
     * @param id of the selected laptop
     */
    showLaptop(id) {
        if (id === -1) {
            this.hideLaptop()
            return
        }
        const selectedLaptop = Laptops.find(function (laptop) {
            return laptop.id === id
        })
        this.price = selectedLaptop.price
        this.name = selectedLaptop.name

        elLaptopName.innerText = this.name
        elLaptopFeaturesText.innerText = selectedLaptop.features
        elLaptopPrice.innerText = this.price + ' Kr'
        elLaptopInformationText.innerText = selectedLaptop.information
        elLaptopImage.src = selectedLaptop.picture

        elFeatureTitle.style.display = ''
        elLaptopInformationBox.style.display = ''
    }

    /**
     * Hides the text that says Features, all the features and the box where the user can read more about the laptop
     * and buy it.
     */
    hideLaptop() {
        elFeatureTitle.style.display = 'none'
        elLaptopInformationBox.style.display = 'none'
        elLaptopFeaturesText.innerText = ''
    }

    /**
     * Attempts to buy the laptop with the current balance.
     * @param bank account (balance and loan) of the user
     */
    buyLaptop(bank) {
        // If the balance is less than the price of the laptop, the attempt to buy is denied.
        if (this.price > bank.getBalance()) {
            alert('You do not have enough money to buy this laptop.')
            return
        } else {
            bank.addBalance(-this.price)
            alert('Congratulations with your new computer. You have just bought the ' + this.name)
        }
        // Hides the laptop information and "resets" the selector.
        this.hideLaptop()
        elSelectLaptop.selectedIndex = -1
    }

}

// Array of laptops
const Laptops = [
    new Laptop(1, 'Brick', 'Jaw shattering performance. No keyboard, no screen. No ram or cpu.',
        1100, 'img/brick.png',
        "It's been through the most gruesome tests and can pride itself in being nigh indestructible. " +
        "Recommended for people who want a consistent performance and a cornerstone in their production"),
    new Laptop(2, 'No-brand laptop', 'Undefined OS, weird keyboard layout, CD-rom. Trackpad works-ish',
        1200, 'img/laptop2.png',
        'Nobody knows how this laptop performs, but everyone who sees it all think that the design is sleek. '
        + 'Recommended for the adventurous who is not afraid to go their own way in the world of operative systems. ' +
        'Linux is free anyway'),
    new Laptop(3, 'Samsung cool looking pc',
        'Sleek design, screen with colors, not weirdly placed fn-button'
        , 1300, 'img/laptop3.png',
        'This laptop has a lot of ram, a lot of monitor space, a sleek design and probably good battery' +
        ' time. It\'s Korean, so perfect for people who love Asian culture'),
    new Laptop(4, 'HP Pavilion with beatsaudio', 'beatsaudio, windows 8, keypad, trackpad, webcam. ' +
        'Weirdly shaped and small up and down arrows.',
        1400, 'img/laptop4.png',
        "This is a HP laptop with top notch audio that you're never going to use since you'll use " +
        'a headset or earplugs anyway. Perfect for those who likes to talk about their sick laptop audio.'),
    new Laptop(5, 'MacBook xx',
        "Apple logo, keyboard, weird screen shape and backlight. No need for flashlight while gaming at night.",
        1500, 'img/macbook.png',
        "The xx stands for unknown and not 20. MacBook is sold for a great price. " +
        "Do not tell George you bought it.")
]
/**
 * Creating new options for the select box based on the array Laptops.
 */
for (const laptop of Laptops) {
    const elLaptopOption = document.createElement('option')
    elLaptopOption.innerText = laptop.name
    elLaptopOption.value = laptop.id
    elSelectLaptop.appendChild(elLaptopOption)
}

export default Laptop


