const elLoanAmount = document.getElementById('loan-amount')
const elLoanText = document.getElementById('loan-text')
const elPayAmount = document.getElementById('pay-amount')

const elLoanMoneyButton = document.getElementById('get-loan-button')
const elPayBackLoanButton = document.getElementById('pay-back-loan-button')

/**
 * Class for Work actions
 */
class Work {
    pay = 0

    /**
     * The user works and adds 100 Kr to their paycheck every time.
     */
        work(){
            this.pay += 100
            elPayAmount.innerText = this.pay + ' Kr'
        }

    /**
     * Moves the user's current paycheck to the bank. If there exists a loan, 10% will be deducted from the paycheck
     * to pay the debt. If there is none, all of the paycheck goes to the balance. Then the paycheck is reset.
     * @param bank account of the user(user's balance and debt)
     */
    moveMoneyToBank(bank){
            const tenPercent = this.pay*0.1
            const ninetyPercent = this.pay*0.9
            if(bank.loanExists()){
                //If the current loan is less than 10% of the paycheck, the rest of the amount will transferred to
                // the user's balance.
                if(bank.getLoan() <= tenPercent){
                    const leftovers = tenPercent-bank.getLoan()
                    bank.payLoan(bank.getLoan())
                    bank.addBalance(leftovers + ninetyPercent)
                    this.resetPay()
                    // Hides the pay back loan button, the debt and the text saying loan, and shows the loan button
                    // since the debt is paid off.
                    this.hideLoanAndShowLoanButton()
                }
                else {
                    bank.addBalance(ninetyPercent)
                    bank.payLoan(tenPercent)
                    this.resetPay()
                }
            }
            else {
                bank.addBalance(this.pay)
                this.resetPay()
            }
        }

    /**
     * Pays off the loan with the user's paycheck. If paycheck is higher than the debt, the difference will be added
     * to the balance and the pay is reset. If the debt is paid off, the pay back button and debt will be hidden and
     * the user can apply for a new loan.
     * @param bank account of the user
     */
    payLoan(bank){
            if(bank.getLoan() <= this.pay) {
                const leftovers = this.pay - bank.getLoan()
                bank.payLoan(bank.getLoan())
                bank.addBalance(leftovers)
                this.resetPay()
                this.hideLoanAndShowLoanButton()
            }
            else {
                bank.payLoan(this.pay)
                this.resetPay()
            }
        }

    /**
     * Resets the pay to 0 Kr
     */
    resetPay(){
            this.pay = 0;
            elPayAmount.innerText = this.pay + ' Kr'
        }

    /**
     * Hides the pay back button and debt, and shows the loan button.
     */
    hideLoanAndShowLoanButton(){
            elLoanText.style.display = 'none'
            elLoanAmount.style.display = 'none'
            elPayBackLoanButton.style.display = 'none'
            elLoanMoneyButton.style.display=''
        }
}

export default Work