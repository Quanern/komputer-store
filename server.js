const express = require('express')
const app = express()
const path = require('path')
const { PORT = 8080 } = process.env

app.use( express.static(path.join(__dirname, 'client')))

app.get('/', (req, res) => {
    return res.sendFile(__dirname + '/client/index.html')
})

app.listen( PORT, () => {
    console.log(`Server has started on port ${PORT}`)
})